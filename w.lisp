(load "g2")

(defvar *indents* (make-hash-table))
(defvar *nl-pos*  (make-hash-table))

(defmacro :>(x y)`(gethash,y,x))
(defmacro geth(x y)`(gethash,y,x))

(gethash "sdgsfdg" *indents*)

{ *indents* :> "sdf" }

(setf { *indents* :> "sdf" } 10)

(defvar indents '(0))
(defvar nl-pos 0)

(defun col-pos (s)
  (- (file-position s) nl-pos))

(defun update-nl-pos (s)
  (setf nl-pos (file-position s)) (^))

(defun enable ()
  (set-macro-character #\Newline 'nl-on) (^))

(defun disable()
  (set-macro-character #\Newline 'nl-off))

(defun nl-off (s c)
  (declare (ignore c))
  (update-nl-pos s))

(defun nl-on (s c)
  (declare (ignore c))
  (update-nl-pos s) (disable)
  (unless (eq (eval (list 'read-t s)) :disable)
    (enable))
  (^))

(defun peekc (s) (peek-char nil s nil))

(defun read-w (s)
  (when (eq (peekc s) #\ )
    (cons (read-char s) (read-w s))))

(defun read-w2 (s)
  (v((ws (read-w s)))
    (case (peekc s)
      (#\Newline (read-char s) (update-nl-pos s) (read-w2 s))
      (t         ws))))

(define-symbol-macro +syntax (enable))
(defun -syntax() (disable))
