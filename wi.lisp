+syntax
load "~/quicklisp/setup"
ql:quickload "clack"         ':silent t
ql:quickload "flexi-streams" ':silent t
import 'flexi-streams::read-byte
νν NL : string #\Newline
νν CT : L ':content-type  "text/html"              ':cache-control "no-cache"
νν CJ : L ':content-type  "application/javascript" ':cache-control "no-cache"
νν CC : L ':content-type  "text/css"               ':cache-control "no-cache"
dƑ s1 (x) : subseq x 1
dƑ ss (x m) :
  ? : {{len x} < m} { x }
      t             { x subseq 0 subseq m }
dƑ read-file (_) :
  v ::infile : open _
      output : apply #'concatenate 'string :
                  σ for line = {infile read-line nil} \
                  while line collect line
     ^ output
dƑ rf (_)    : L 200 CT : L {read-file _} NL
dƑ rs (_ __) : L __  CT : L            _  NL
clack:clackup :
  λ (env) :
    v ::path-info  { env getf ':path-info }
        method     { env getf ':request-method }
        raw-body   { env getf ':raw-body }
        clack-io   { env getf ':clack-io }
        clen       { env getf ':content-length }
       ? : { method eq ':get } :
             ? : {path-info equal "/"} :
                     pr "s/index.html~%"
                     rf "s/index.html"
                 {{path-info ss 3} equal "/f/"} :
                     pr "FFF:~s~%" path-info
                     rf : s1 path-info
                 {{path-info ss 3} equal "/s/"} :
                     pr "SSS:~s~%" path-info
                     rf : s1 path-info
                 t : rs "not found" 404
           { method eq ':post } :
             ? : {{path-info ss 3} equal "/f/"} :
                     v ::f : open {s1 path-info} ':direction \
                               ':output ':if-exists ':supersede
                        write-string :
                          concatenate 'string :
                            σ for n from 0 below clen collect :
                              code-char : read-byte raw-body
                          ^ f
                        finish-output f
                        rs "true" 200
                 t : rs "not found" 404
  ^ ':server
  ^ ':woo
  ^ ':use-default-middlewares
  ^ nil
-syntax
(sleep 360000)
